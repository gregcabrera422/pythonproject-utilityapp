from bs4 import BeautifulSoup
import requests
import urllib.request
import os

response = 'y'

while(response == 'y'):
	imageName = input("What image do you want? ")
	limit = input("How many images to download? ")

	url="https://www.google.co.in/search?q="+imageName+"&source=lnms&tbm=isch"
	r = requests.get(url)
	content = r.text
	soup = BeautifulSoup(content, "html.parser")

	images = []
	images = soup.findAll('img')
		
	counter = 1

	os.mkdir(imageName)

	for image in images:
		print("Downloading " + image.get('src'))
		urllib.request.urlretrieve(image.get('src'),  imageName + "/" + imageName + str(counter) + ".jpg")
		
		if counter == int(limit):
			break;

		counter += 1

	response = input("Do you want to download again? [Y/N] ").lower()
