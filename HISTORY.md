Version 0:

- Made a dictionary using Oxford Dictionaries Api.

Version 1:

- Added word definition and example of sentence.
- Added error handling.

Version 2:

- Replaced idea. Made a web scraping script.
- Made a web scraping script that uses BeautifulSoup.
- Downloads images from google depending on user input

Version 3:

- Changed file name.
- Added directory corresponding to filename.